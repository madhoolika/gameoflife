package com.twpathashala51;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

//Represents a State of the game of life
public class GameBoard {
    private final Set<Cell> liveCells;

    public GameBoard(Set<Cell> liveCells) {
        this.liveCells = liveCells;
    }

    public GameBoard generateNextState() {
        Set<Cell> liveCellsInNextState = new HashSet<>();
        Iterator initialBoardIterator = this.iterator();
        while (initialBoardIterator.hasNext()) {
            Cell cell = (Cell) initialBoardIterator.next();
            Set<Cell> nextStateOfCellAndNeighbours = cell.getNextStateOfCellAndNeighbours(this);
            liveCellsInNextState.addAll(nextStateOfCellAndNeighbours);
        }
        return new GameBoard(liveCellsInNextState);
    }

    public boolean contains(Cell cell) {
        return this.liveCells.contains(cell);
    }

    private Iterator iterator() {
        return this.liveCells.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameBoard gameBoard = (GameBoard) o;

        return liveCells != null ? liveCells.equals(gameBoard.liveCells) : gameBoard.liveCells == null;

    }

    @Override
    public int hashCode() {
        return liveCells != null ? liveCells.hashCode() : 0;
    }

    @Override
    public String toString() {
        return ""  + liveCells;
    }
}