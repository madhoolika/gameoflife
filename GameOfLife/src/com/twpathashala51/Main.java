
package com.twpathashala51;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static com.twpathashala51.Cell.cell;

//Driver class to take user input from console and give the output
public class Main {
    public static void main(String[] args) {
        Set<Cell> initialState = new HashSet<>();
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("                        Welcome To The Game Of Life");
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("Enter the number of Games you want to play: ");
        Scanner in = new Scanner(System.in);
        int numberOfGames = in.nextInt();

        for (int i = 0; i < numberOfGames; i++) {
            System.out.println("\nGame " + (i + 1) + ":");
            try {
                initialState = getInitialState();
            }
            catch (InvalidInputTypeException e){
                System.out.println(e);
            }
            GameBoard initialBoardState = new GameBoard(initialState);
            GameBoard generatedNextState = initialBoardState.generateNextState();
            System.out.println("The Generated Board: " + generatedNextState.toString());
        }
    }

    private static Set<Cell> getInitialState() throws InvalidInputTypeException {
        int xCoordinate, yCoordinate;
        String cell;
        String[] cellCoordinates;
        Set<Cell> initialState = new HashSet<>();
        Scanner in = new Scanner(System.in);

        System.out.println("Enter the number of Cells the Grid Has: ");
        int numberOfCells = in.nextInt();

        System.out.println("Enter the Cells: ");
        in = new Scanner(System.in);

        for (int j = 0; j < numberOfCells; j++) {
            cell = in.nextLine();
            if(cell.contains(".")){
                throw new InvalidInputTypeException("The Input given" + cell + " is of invalid type");
            }
            cellCoordinates = cell.split(", ");
            xCoordinate = Integer.parseInt(cellCoordinates[0]);
            yCoordinate = Integer.parseInt(cellCoordinates[1]);
            Cell nextCell = cell(xCoordinate, yCoordinate);
            initialState.add(nextCell);
        }
        return initialState;
    }
}