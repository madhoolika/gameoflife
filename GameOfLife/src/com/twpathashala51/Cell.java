package com.twpathashala51;

import java.util.HashSet;
import java.util.Set;

//Represents a location in the infinite two-dimensional orthogonal grid (Game Board)
public class Cell {
    private int xCoordinate;
    private int yCoordinate;
    private boolean isAlive;

    private Cell(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.isAlive = true;
    }

    public static Cell cell(int x, int y) {
        return new Cell(x, y);
    }

    public Set<Cell> getNextStateOfCellAndNeighbours(GameBoard presentBoard) {
        Set<Cell> cellAndItsNeighbours = new HashSet<>();
        Set<Cell> liveCellsInNextBoard = new HashSet<>();

        cellAndItsNeighbours.add(this);
        cellAndItsNeighbours = getNeighbours(this);

        for (Cell thisCell : cellAndItsNeighbours) {
            if (!thisCell.isAlive) {
                continue;
            }
            if (thisCell.isCellLiveInNextBoard(presentBoard)) {
                liveCellsInNextBoard.add(thisCell);
                thisCell.isAlive = true;
            }
            else {
                thisCell.isAlive = false;
            }
        }
        return liveCellsInNextBoard;
    }

    private Set<Cell> getNeighbours(Cell cell) {
        int x = cell.xCoordinate;
        int y = cell.yCoordinate;
        HashSet<Cell> neighbours = new HashSet<>();

        neighbours.add(cell(x - 1, y));
        neighbours.add(cell(x + 1, y));
        neighbours.add(cell(x, y - 1));
        neighbours.add(cell(x, y + 1));
        neighbours.add(cell(x - 1, y - 1));
        neighbours.add(cell(x + 1, y + 1));
        neighbours.add(cell(x - 1, y + 1));
        neighbours.add(cell(x + 1, y - 1));

        return neighbours;

    }

    private boolean isCellLiveInNextBoard(GameBoard presentBoard) {
        int numberOfNeighbours;
        numberOfNeighbours = this.getNumberOfLiveNeighbours(presentBoard);
        if (presentBoard.contains(this) && (numberOfNeighbours == 2 || numberOfNeighbours == 3)) {
            return true;
        }
        return !(presentBoard.contains(this)) && numberOfNeighbours == 3;
    }

    private int getNumberOfLiveNeighbours(GameBoard presentBoard) {
        int numberOfNeighbours = 0;
        Set<Cell> neighbours = getNeighbours(cell(xCoordinate, yCoordinate));

        for(Cell thisCell : neighbours) {
            if(presentBoard.contains(thisCell)) {
                numberOfNeighbours++;
            }
        }
        return numberOfNeighbours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        return xCoordinate == cell.xCoordinate && yCoordinate == cell.yCoordinate;
    }

    @Override
    public int hashCode() {
        int result = xCoordinate;
        result = 31 * result + yCoordinate;
        return result;
    }

    @Override
    public String toString() {
        return "(" + xCoordinate + ", " + yCoordinate + ")";
    }
}