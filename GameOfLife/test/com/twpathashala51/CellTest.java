package com.twpathashala51;

import org.junit.Assert;
import org.junit.Test;

public class CellTest {
    @Test
    public void checkEqualityOfTwoCellsWithSameCoordinates() {
        Assert.assertEquals(Cell.cell(0, 0), Cell.cell(0, 0));
    }

    @Test
    public void checkInequalityOfTwoCellsWithDifferentCoordinates() {
        Assert.assertNotEquals(Cell.cell(0, 1), Cell.cell(9, 10));
    }
}
