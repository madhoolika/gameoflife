package com.twpathashala51;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.twpathashala51.Cell.cell;
import static org.junit.Assert.assertEquals;

public class GameOfLifeTest {

    @Test
    public void stillLifeTest() {
        Set<Cell> blockPattern = blockPattern();
        GameBoard initialState = new GameBoard(blockPattern);
        GameBoard expectedNextState = new GameBoard(blockPattern);
        GameBoard generatedNextState = initialState.generateNextState();
        assertEquals(expectedNextState, generatedNextState);
    }

    private Set<Cell> blockPattern() {
        Set<Cell> initialSate = new HashSet<>();
        initialSate.add(cell(1, 1));
        initialSate.add(cell(1, 2));
        initialSate.add(cell(2, 1));
        initialSate.add(cell(2, 2));

        return initialSate;
    }

    @Test
    public void boatPatternTest() {
        Set<Cell> initialState = new HashSet<>();
        Set<Cell> expectedState = new HashSet<>();

        initialState.add(cell(0, 1));
        initialState.add(cell(1, 0));
        initialState.add(cell(2, 1));
        initialState.add(cell(0, 2));
        initialState.add(cell(1, 2));
        expectedState.add(cell(0, 1));
        expectedState.add(cell(1, 0));
        expectedState.add(cell(2, 1));
        expectedState.add(cell(0, 2));
        expectedState.add(cell(1, 2));

        GameBoard initialBoardState = new GameBoard(initialState);

        GameBoard expectedBoardState = new GameBoard(expectedState);

        GameBoard generatedNextState = initialBoardState.generateNextState();

        assertEquals(expectedBoardState, generatedNextState);
    }

    @Test
    public void blinkerPatternTest() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();

        seed.add(cell(1, 1));
        seed.add(cell(1, 0));
        seed.add(cell(1, 2));
        expectedTick.add(cell(1, 1));
        expectedTick.add(cell(0, 1));
        expectedTick.add(cell(2, 1));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void twoPhaseOscillatorTest() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();

        seed.add(cell(1, 1));
        seed.add(cell(1, 2));
        seed.add(cell(1, 3));
        seed.add(cell(2, 2));
        seed.add(cell(2, 3));
        seed.add(cell(2, 4));
        expectedTick.add(cell(0, 2));
        expectedTick.add(cell(1, 1));
        expectedTick.add(cell(1, 4));
        expectedTick.add(cell(2, 1));
        expectedTick.add(cell(2, 4));
        expectedTick.add(cell(3, 3));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void emptyGAmeBoardShouldReturnAnEmptyGameBoard() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void testTwoByTwoGridWithThreeLiveCellsShouldGiveAnAllLiveGrid() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();
        seed.add(cell(0, 1));
        seed.add(cell(1, 0));
        seed.add(cell(1, 1));
        expectedTick.add(cell(0, 0));
        expectedTick.add(cell(0, 1));
        expectedTick.add(cell(1, 0));
        expectedTick.add(cell(1, 1));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void gameBoardWithTwoDiagonalCellsShouldGiveEmptyGmeBoard() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();
        seed.add(cell(0, 0));
        seed.add(cell(1, 1));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void gameBoardWithManyDiagonalCellsShouldRemoveCellsInEitherEnds() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();
        seed.add(cell(0, 2));
        seed.add(cell(1, 1));
        seed.add(cell(2, 0));
        expectedTick.add(cell(1, 1));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void gameBoardWithOneLiveCellReturnsEmptyGameBoard() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();
        seed.add(cell(1, 1));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void testAThreeByThreeSquareMatrixInTheGameBoard() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();
        seed.add(cell(0, 0));
        seed.add(cell(0, 1));
        seed.add(cell(0, 2));
        seed.add(cell(1, 0));
        seed.add(cell(1, 1));
        seed.add(cell(1, 2));
        seed.add(cell(2, 0));
        seed.add(cell(2, 1));
        seed.add(cell(2, 2));
        expectedTick.add(cell(0, 0));
        expectedTick.add(cell(0, 2));
        expectedTick.add(cell(2, 2));
        expectedTick.add(cell(1, 3));
        expectedTick.add(cell(-1, 1));
        expectedTick.add(cell(1, -1));
        expectedTick.add(cell(2, 0));
        expectedTick.add(cell(3, 1));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }

    @Test
    public void testGameBoardWithScatteredCoordinates() {
        Set<Cell> seed = new HashSet<>();
        Set<Cell> expectedTick = new HashSet<>();
        seed.add(cell(500, 500));
        seed.add(cell(501, 499));
        seed.add(cell(502, 499));
        seed.add(cell(502, 500));
        seed.add(cell(502, 502));
        seed.add(cell(502, 503));
        seed.add(cell(503, 502));

        expectedTick.add(cell(503, 503));
        expectedTick.add(cell(503, 502));
        expectedTick.add(cell(503, 501));
        expectedTick.add(cell(502, 499));
        expectedTick.add(cell(502, 500));
        expectedTick.add(cell(502, 501));
        expectedTick.add(cell(502, 502));
        expectedTick.add(cell(501, 501));
        expectedTick.add(cell(501, 499));
        expectedTick.add(cell(502, 503));

        GameBoard initialState = new GameBoard(seed);

        GameBoard expectedNextState = new GameBoard(expectedTick);

        GameBoard generatedNextState = initialState.generateNextState();

        assertEquals(expectedNextState, generatedNextState);
    }
}